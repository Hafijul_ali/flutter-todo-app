import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/hive_flutter.dart';
import '../../core/constants/routes.dart';

import '../../init.dart';
import '../../screens/add_todo_page/add_todo_page.dart';
import '../../storage/models/todo_model.dart';
import '../../utils/date_time_utils.dart';
import '../navigation/navigate.dart';
import '../widgets/app_bar.dart';
import '../widgets/nav_bar.dart';
import '../widgets/snack_bar.dart';

StreamBuilder<BoxEvent> listBuilder(
  BuildContext context,
  Box<Todo>? data,
) {
  return StreamBuilder<BoxEvent>(
    stream: settingsDatabase!.watch(),
    builder: (BuildContext context, AsyncSnapshot<BoxEvent> snapshot) {
      return ValueListenableBuilder<Box<Todo>>(
        valueListenable: data!.listenable(),
        builder: (BuildContext context, Box<Todo> todo, _) {
          if (data.isEmpty) {
            return _emptyList(context);
          } else {
            return _list(context, data);
          }
        },
      );
    },
  );
}

IconButton _copyButton(String title, String content) {
  return IconButton(
    onPressed: () async {
      await Clipboard.setData(ClipboardData(text: '$title\n$content'));
      showSnackbar(
        'Copied $title to clipboard',
      );
    },
    icon: const Icon(Icons.copy_outlined),
  );
}

IconButton _doneButton(Todo? todo, int index) {
  return IconButton(
    onPressed: () async {
      todo!.done = true;
      upcomingTodoDatabase?.deleteAt(index);
      doneTodoDatabase!.add(todo);
      showSnackbar(
        'Marked todo ${todo.title} as done',
      );
    },
    icon: const Icon(Icons.done_outlined),
  );
}

IconButton _editButton(BuildContext context, int index) {
  return IconButton(
    onPressed: () async {
      currentPath = editTodoPageRoute;
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Scaffold(
              appBar: appBar(context),
              body: AddTodo(
                index: index,
              ),
              bottomNavigationBar: navBar(1, (_) => safePop(context)),
            ),
          ));
    },
    icon: const Icon(Icons.edit_outlined),
  );
}

Widget _emptyList(BuildContext context) {
  return Center(
    child:
        Text('Nothing to do... Great!', style: TextStyle(fontSize: fontSize)),
  );
}

Widget _item(
  BuildContext context,
  Todo? todo,
  int index,
) {
  final String titleText =
      '${todo!.title}  ${getTimeFromTimeOfDay(todo.time)}  ${getDateFromDateTime(todo.date)}';
  return ListTile(
    leading: _leadingButton(todo.title, index),
    title: _title(titleText),
    subtitle: _subtitle(todo.content),
    isThreeLine: true,
    trailing: Wrap(spacing: 5, children: <IconButton>[
      _editButton(context, index),
      _copyButton(todo.title, todo.content),
      (currentPath == '/DoneTodoPage')
          ? _undoButton(todo, index)
          : _doneButton(todo, index),
    ]),
  );
}

IconButton _leadingButton(String title, int index) {
  Box<Todo> database = upcomingTodoDatabase!;

  if (currentPath == doneTodoPageRoute) {
    database = doneTodoDatabase!;
  }
  return IconButton(
    onPressed: () async {
      await database.deleteAt(index);
      showSnackbar(
        'Deleted todo $title successfully',
      );
    },
    icon: const Icon(
      Icons.delete_outlined,
      color: Colors.redAccent,
    ),
  );
}

ListView _list(BuildContext context, Box<Todo>? data) {
  return ListView.builder(
    shrinkWrap: true,
    itemCount: data!.length,
    itemBuilder: (BuildContext context, int index) {
      Todo? todo = data.getAt(index);
      return _item(context, todo, index);
    },
  );
}

Text _subtitle(String text) {
  return Text(
    text,
    style: TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: fontSize,
    ),
  );
}

Text _title(String text) {
  return Text(
    text,
    style: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: fontSize,
    ),
    maxLines: 3,
  );
}

IconButton _undoButton(Todo? todo, int index) {
  return IconButton(
    onPressed: () async {
      todo!.done = false;
      upcomingTodoDatabase?.add(todo);
      doneTodoDatabase?.deleteAt(index);
      showSnackbar(
        'Marked todo ${todo.title} as undone',
      );
    },
    icon: const Icon(Icons.undo_outlined),
  );
}
