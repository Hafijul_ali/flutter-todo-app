const String failureMessage = 'Failure';
const String successMessage = 'Success';
const String undo = 'UNDO';
const String addTodo = 'Add Todo';
const String editTodo = 'Edit Todo';
const String upcomingTodo = 'Upcoming Todo';
const String doneTodo = 'Done Todo';
const String writePermissionDenied = 'Permission denied to write storage';
const String apiKeyNotSet = 'API key not set, cannot sync to cloud';
const String apiKeySaved = 'API key saved successfully';
const String apiKeySaveError = 'Error saving API key';
const String cannotConnectToCloud = 'Error connecting to cloud';
const String connectedToCloud = 'Connected to cloud';
const String ok = 'OK';
