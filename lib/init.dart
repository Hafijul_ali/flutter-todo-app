import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:timezone/data/latest_all.dart' as tz_latest;
import 'package:timezone/timezone.dart' as tz;

import 'core/constants/app.dart';
import 'core/constants/routes.dart';
import 'core/constants/settings.dart';
import 'screens/add_todo_page/add_todo_page.dart';
import 'screens/done_todo_page/done_todo_page.dart';
import 'screens/home_page/home_page.dart';
import 'screens/login_page/login_page.dart';
import 'screens/settings_page/settings_page.dart';
import 'screens/sign_in_page/sign_in_page.dart';
import 'screens/upcoming_todo_page/upcoming_todo_page.dart';
import 'services/cloud/cloud.dart';
import 'services/cloud/providers/base/factory.dart';
import 'services/cloud/providers/todo_api/todo_api.dart';
import 'services/notification/notification.dart';
import 'storage/models/todo_model.dart';

Box<Todo>? upcomingTodoDatabase;
Box<Todo>? doneTodoDatabase;
Box<dynamic>? settingsDatabase;
// WARN: This `secureDatabase` object is only for compatibility reasons.
// As of 2025 there is no stable cross-paltform solution to securely store data.
// Flutter Secure Storage doesn't work well on Web, which is a deal-breaker.
// Web version of this app is hosted publicly, and needs secure way to login and access data.
Box<dynamic>? secureDatabase;
PermissionStatus? permission;
String? upcomingTodoDatabaseFilePath;
String? doneTodoDatabaseFilePath;
String? settingsDatabaseFilePath;
Directory? databaseDirectory;
FlutterLocalNotificationsPlugin? notification;

// INFO : Primarily used to navigate widget tree from MaterialApp
final GlobalKey<ScaffoldMessengerState> rootScaffoldMessengerKey =
    GlobalKey<ScaffoldMessengerState>();
TextEditingController searchTextController = TextEditingController();
TextEditingController apiKeyTextController = TextEditingController();
int currentPageIndex =
    settingsDatabase?.get(landingPageSettingsKey, defaultValue: 0);
String currentPath = '/HomePage';
PageController pageController = PageController(initialPage: currentPageIndex);
ThemeMode appThemeMode =
    settingsDatabase?.get(appThemeSettingsKey, defaultValue: ThemeMode.system);
bool useMaterial3 =
    settingsDatabase?.get(material3SettingsKey, defaultValue: true);
double fontSize =
    settingsDatabase?.get(fontSizeSettingsKey, defaultValue: 30.0);

String? apiKey = settingsDatabase?.get(apiKeySettingsKey, defaultValue: '');
String cloudProvider =
    settingsDatabase?.get(cloudProviderSettingsKey, defaultValue: 'todo-api');
Cloud cloud = Cloud(provider: cloudProvider);

Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
  upcomingTodoPageRoute: (_) => const UpcomingTodo(),
  addTodoPageRoute: (_) => const AddTodo(),
  doneTodoPageRoute: (_) => const DoneTodo(),
  homePageRoute: (_) => const HomePage(),
  settingsPageRoute: (_) => const SettingsPage(),
  editTodoPageRoute: (_) => const AddTodo(
        index:
            0, // INFO : setting index to dummy value so that navigator for EditTodoPage works correctly
      ),
  signinPageRoute: (_) => const SigninPage(),
  loginPageRoute: (_) => LoginPage(),
};

Map<String, Widget> tabs = <String, Widget>{
  upcomingTodoPageRoute: const UpcomingTodo(),
  addTodoPageRoute: const AddTodo(),
  doneTodoPageRoute: const DoneTodo(),
};

Future<void> initApp() async {
  await _initServices();
  await _initDatabase();
  await _initCloud();
}

Future<void> _initCloud() async {
  await settingsDatabase?.put(apiKeySettingsKey, apiKey);
  await cloud.login(apiKey: settingsDatabase?.get(apiKeySettingsKey));
}

Future<void> _initServices() async {
  WidgetsFlutterBinding.ensureInitialized();

  TodoProviderFactory.registerProvider('todo-api', () => TodoApiProvider());

  if (!kIsWeb) {
    tz_latest.initializeTimeZones();
    tz.setLocalLocation(tz.getLocation('Asia/Kolkata'));
  }

  notification = FlutterLocalNotificationsPlugin();
  await notification!.initialize(initializationSettings);
}

Future<void> _initDatabase() async {
  await Hive.initFlutter(appName);
  Hive
    ..registerAdapter(TodoAdapter())
    ..registerAdapter(TimeOfDayAdapter());
  upcomingTodoDatabase = await Hive.openBox<Todo>(upcomingTodoDatabaseFileName);
  doneTodoDatabase = await Hive.openBox<Todo>(doneTodoDatabaseFileName);
  settingsDatabase = await Hive.openBox<dynamic>(settingsDatabaseFileName);
  upcomingTodoDatabaseFilePath = upcomingTodoDatabase!.path.toString();
  doneTodoDatabaseFilePath = doneTodoDatabase!.path.toString();
  settingsDatabaseFilePath = settingsDatabase!.path.toString();

  if (!kIsWeb) {
    databaseDirectory = Directory(upcomingTodoDatabaseFilePath!).parent;
  }
}

ThemeData getTheme() {
  if (ThemeMode.system == ThemeMode.dark) {
    return ThemeData.dark(useMaterial3: useMaterial3);
  }
  return ThemeData.light(useMaterial3: useMaterial3);
}
