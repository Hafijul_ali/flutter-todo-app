import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final LinuxInitializationSettings linuxInitializationSettings =
    LinuxInitializationSettings(
  defaultActionName: 'Open notification',
  defaultIcon: AssetsLinuxIcon('icons/app_icon.png'),
);

const LinuxNotificationDetails linuxNotificationDetails =
    LinuxNotificationDetails(
  actions: <LinuxNotificationAction>[
    LinuxNotificationAction(
      key: 'id_1',
      label: 'Action 1',
    ),
    LinuxNotificationAction(
      key: 'id_3',
      label: 'Action 2',
    ),
  ],
);
