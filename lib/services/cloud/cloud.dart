import 'providers/base/factory.dart';
import 'providers/base/provider.dart';
import 'providers/todo_api/todo.dart';

class Cloud {
  final TodoProvider _provider;

  Cloud({required String provider})
      : _provider = TodoProviderFactory.getProvider(provider);

  Future<dynamic> login(
      {String? username, String? password, String? apiKey}) async {
    return await _provider.login(username: username, password: password);
  }

  Future<void> logout() async {
    return await _provider.logout();
  }

  Future<dynamic> getTodo(String id) async {
    return await _provider.getTodo(id);
  }

  Future<dynamic> postTodo(Todo todo) async {
    return await _provider.postTodo(todo);
  }
}
