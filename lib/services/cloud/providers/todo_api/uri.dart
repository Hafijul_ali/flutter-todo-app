class TodoApiUri {
  static const String scheme = 'https';
  static const String host = 'apis.hafijulali.com';
  static const String base = '${TodoApiUri.scheme}://${TodoApiUri.host}';
  static const String service = '${TodoApiUri.base}/todo-api';
  static const String apiVersion = '${TodoApiUri.service}/api/v1';
  static const String todo = '${TodoApiUri.apiVersion}/todo';
  static const String signin = '${TodoApiUri.apiVersion}/signin';
}
