import '../../client/apiclient.dart';
import '../base/provider.dart';
import 'options.dart';
import 'todo.dart';

class TodoApiProvider implements TodoProvider {
  final Todo _todo = Todo();
  final DioApiClient _client = DioApiClient(baseOptions);

  @override
  Future getTodo(String id) async {
    return await _client.get('/');
  }

  @override
  Future postTodo(Todo todo) async {
    return await _client.post('/todo');
  }

  @override
  Future<dynamic> login(
      {String? username, String? password, String? apiKey}) async {
    return await _todo.login(apiKey);
  }

  @override
  Future<void> logout() async {
    return await _todo.logout();
  }
}
