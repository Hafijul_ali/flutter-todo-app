class ApiResponse {
  final int statusCode;
  final String message;
  final dynamic data;
  final dynamic error;
  const ApiResponse(this.statusCode, this.message, this.data, this.error);

  factory ApiResponse.fromJson(Map<String, dynamic> json) {
    return ApiResponse(
        json['status_code'], json['message'], json['data'], json['error']);
  }
}
