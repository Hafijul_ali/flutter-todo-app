import 'provider.dart';

class TodoProviderFactory {
  static final Map<String, TodoProvider Function()> _providers = {};

  static void registerProvider(
      String name, TodoProvider Function() constructor) {
    _providers[name] = constructor;
  }

  static TodoProvider getProvider(String name) {
    final constructor = _providers[name];
    if (constructor != null) {
      return constructor();
    }
    throw UnimplementedError('Provider not found for: $name');
  }
}
