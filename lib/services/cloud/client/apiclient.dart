import 'package:dio/dio.dart';

abstract class ApiClient {
  Future<Map<String, dynamic>> get(String url, {Map<String, String>? headers});
  Future<Map<String, dynamic>> post(String url,
      {Map<String, String>? headers, dynamic body});
  Future<Map<String, dynamic>> put(String url,
      {Map<String, String>? headers, dynamic body});
  Future<Map<String, dynamic>> delete(String url,
      {Map<String, String>? headers});
}

class DioApiClient implements ApiClient {
  final Dio _dio;

  DioApiClient(BaseOptions baseOptions) : _dio = Dio(baseOptions);

  @override
  Future<Map<String, dynamic>> get(String url,
      {Map<String, String>? headers}) async {
    final response = await _dio.get(url, options: Options(headers: headers));
    return response.data;
  }

  @override
  Future<Map<String, dynamic>> post(String url,
      {Map<String, String>? headers, dynamic body}) async {
    final response =
        await _dio.post(url, data: body, options: Options(headers: headers));
    return response.data;
  }

  @override
  Future<Map<String, dynamic>> put(String url,
      {Map<String, String>? headers, dynamic body}) async {
    final response =
        await _dio.put(url, data: body, options: Options(headers: headers));
    return response.data;
  }

  @override
  Future<Map<String, dynamic>> delete(String url,
      {Map<String, String>? headers}) async {
    final response = await _dio.delete(url, options: Options(headers: headers));
    return response.data;
  }
}
