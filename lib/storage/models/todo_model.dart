import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import '../../utils/date_time_utils.dart';

part 'todo_model.g.dart';

@HiveType(typeId: 0)
class Todo {
  Todo({
    required this.title,
    required this.time,
    required this.date,
    required this.content,
    required this.done,
  });

  @HiveField(0)
  String title;

  @HiveField(1)
  TimeOfDay time;

  @HiveField(2)
  DateTime date;

  @HiveField(3)
  String content;

  @HiveField(4)
  bool done = false;

  Todo.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        time = getTimeOfDayFromTime(json['time']),
        date = getDateTimeFromDate(json['date']),
        content = json['content'],
        done = json['done'];

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'time': getTimeFromTimeOfDay(time),
      'date': getDateFromDateTime(date),
      'content': content,
      'done': done,
    };
  }
}
