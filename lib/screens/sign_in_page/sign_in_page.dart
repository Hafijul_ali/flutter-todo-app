import 'package:flutter/material.dart';

import '../../core/constants/settings.dart';
import '../../custom/widgets/app_bar.dart';
import '../../init.dart';

class SigninPage extends StatelessWidget {
  const SigninPage({super.key});

  TextFormField _apiKeyField() {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Enter backend API Key',
        border: OutlineInputBorder(),
      ),
      controller: apiKeyTextController,
      validator: (String? text) =>
          text!.isEmpty ? 'API key must not be empty' : null,
    );
  }

  Widget _add(BuildContext context, String text) {
    return ElevatedButton(
        onPressed: () =>
            {secureDatabase?.put(apiKeySettingsKey, apiKeyTextController.text)},
        child: Text(text));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context),
      body: Center(
        child: Column(children: [_apiKeyField(), _add(context, 'Save')]),
      ),
    );
  }
}
