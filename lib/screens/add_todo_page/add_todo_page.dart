import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:uuid/uuid.dart';

import '../../custom/widgets/snack_bar.dart';
import '/init.dart';
import '../../custom/widgets/alert_dialog.dart';
import '../../storage/models/todo_model.dart';
import '../../utils/date_time_utils.dart';

class AddTodo extends StatefulWidget {
  final int? index;
  const AddTodo({super.key, this.index});

  @override
  _AddTodoState createState() => _AddTodoState();
}

class _AddTodoState extends State<AddTodo> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController titleController = TextEditingController();
  final TextEditingController timeController = TextEditingController();
  final TextEditingController dateController = TextEditingController();
  final TextEditingController contentController = TextEditingController();

  bool isEditMode = false;
  bool isFormDirty = false;

  Box<Todo>? database = upcomingTodoDatabase;
  int? get todoIndex => widget.index;

  void addTodo() async {
    try {
      if (_formKey.currentState!.validate()) {
        final String title = titleController.text;
        final TimeOfDay time = timeController.text.isEmpty
            ? TimeOfDay.now()
            : getTimeOfDayFromTime(timeController.text);
        final DateTime date = dateController.text.isEmpty
            ? DateTime.now()
            : getDateTimeFromDate(dateController.text);
        final String content = contentController.text;
        _formKey.currentState!.save();

        final Todo todo = Todo(
          title: title,
          time: time,
          date: date,
          content: content,
          done: false,
        );
        if (isEditMode == true && todoIndex != null) {
          await database!.putAt(todoIndex!, todo);
          showSnackbar(
            'Todo $title updated',
          );
        } else {
          await database?.add(todo);
          showSnackbar(
            'Todo $title added',
          );
        }
      }
      clearAllFields();
    } catch (e) {
      showSnackbar(
        'Error saving todo',
      );
    }
  }

  Widget addTodoForm(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: ListView(children: _formElements(context)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    currentPath = '/AddTodoPage';
    return addTodoForm(context);
  }

  void clearAllFields() {
    setState(() {
      titleController.text = timeController.text =
          dateController.text = contentController.text = '';
      isFormDirty = false;
    });
  }

  Future<bool> confirmDiscardTodo() async {
    final String? response = await showAlertDialog(
      context,
      'Confirm',
      'Are you sure to discard the todo?',
    );
    if (response == 'OK') {
      showSnackbar(
        'Todo discarded',
      );
      clearAllFields();
      setState(() => isFormDirty = false);
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    if (todoIndex != null) {
      isEditMode = true;
      if (tabs.keys.toList().elementAt(currentPageIndex) == '/DoneTodoPage') {
        database = doneTodoDatabase!;
      }
      final Todo? todo = database?.getAt(todoIndex!);
      titleController.text = todo!.title;
      timeController.text = getTimeFromTimeOfDay(todo.time);
      dateController.text = getDateFromDateTime(todo.date);
      contentController.text = todo.content;
      isFormDirty = true;
    }
    super.initState();
  }

  Widget _add(BuildContext context, String text) {
    return ElevatedButton(onPressed: addTodo, child: Text(text));
  }

  ElevatedButton _cancel(BuildContext context) {
    return ElevatedButton(
      child: const Text('CANCEL'),
      onPressed: () async {
        if (isFormDirty == true) {
          confirmDiscardTodo();
        }
      },
    );
  }

  TextFormField _contentField() {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Content',
        border: OutlineInputBorder(),
      ),
      controller: contentController,
      keyboardType: TextInputType.multiline,
      maxLines: 5,
    );
  }

  Widget _dateField(BuildContext context) {
    return GestureDetector(
      onTap: () => _selectDate(context),
      child: AbsorbPointer(
        child: TextFormField(
          decoration: const InputDecoration(
            labelText: 'Date',
            border: OutlineInputBorder(),
          ),
          controller: dateController,
        ),
      ),
    );
  }

  List<Widget> _formElements(BuildContext context) {
    return <Widget>[
      Form(
        key: _formKey,
        onChanged: () => setState(() => isFormDirty = true),
        onWillPop: () {
          if (isFormDirty) {
            return confirmDiscardTodo();
          } else {
            return Future.delayed(const Duration(milliseconds: 50));
          }
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _inputFields(context),
        ),
      ),
      Row(
        children: _inputButtons(context),
      ),
    ];
  }

  List<Widget> _inputButtons(BuildContext context) {
    return <Widget>[
      if (isEditMode) _add(context, 'UPDATE') else _add(context, 'ADD'),
      const SizedBox(width: 16),
      _cancel(context),
    ];
  }

  List<Widget> _inputFields(BuildContext context) {
    return <Widget>[
      _titleField(),
      const SizedBox(height: 16),
      _timeField(context),
      const SizedBox(height: 16),
      _dateField(context),
      const SizedBox(height: 16),
      _contentField(),
      const SizedBox(height: 16),
    ];
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1901),
      lastDate: DateTime(2100),
    );
    if (picked != null) {
      setState(() {
        dateController.text = getDateFromDateTime(picked);
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    );
    if (picked != null) {
      setState(() {
        timeController.text = getTimeFromTimeOfDay(picked);
      });
    }
  }

  Widget _timeField(BuildContext context) {
    return GestureDetector(
      onTap: () => _selectTime(context),
      child: AbsorbPointer(
        child: TextFormField(
          decoration: const InputDecoration(
            labelText: 'Time',
            border: OutlineInputBorder(),
          ),
          controller: timeController,
          keyboardType: TextInputType.datetime,
        ),
      ),
    );
  }

  TextFormField _titleField() {
    return TextFormField(
      decoration: const InputDecoration(
        labelText: 'Title',
        border: OutlineInputBorder(),
      ),
      textCapitalization: TextCapitalization.sentences,
      controller: titleController,
      validator: (String? text) =>
          text!.isEmpty ? 'Title must not be empty' : null,
    );
  }
}
