import 'package:flutter/material.dart';

import '../../custom/components/list_builder.dart';
import '../../init.dart';

class DoneTodo extends StatelessWidget {
  const DoneTodo({super.key});

  @override
  Widget build(BuildContext context) {
    currentPath = '/DoneTodoPage';
    return listBuilder(context, doneTodoDatabase);
  }
}
