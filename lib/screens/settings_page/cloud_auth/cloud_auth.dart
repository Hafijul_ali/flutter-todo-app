import 'package:flutter/material.dart';

import '../../../core/constants/app.dart';
import '../../../core/constants/settings.dart';
import '../../../core/constants/strings.dart';
import '../../../custom/widgets/snack_bar.dart';
import '../../../init.dart';

ListTile cloudAuthentication(BuildContext context) {
  return ListTile(
    leading: const Icon(Icons.security_outlined),
    title: const Text('Cloud API Key'),
    subtitle: const Text('API key to authenticate with backend service'),
    trailing: SizedBox(
      width: settingsTileWidgetWidth,
      child: TextFormField(
        onEditingComplete: () {
          try {
            settingsDatabase?.put(apiKeySettingsKey, apiKeyTextController.text);
            showSnackbar(apiKeySaved, null);
          } catch (e) {
            showSnackbar(apiKeySaveError, null);
          }
        },
        decoration: InputDecoration(
          hintText: apiKey!.isNotEmpty ? '*********' : '',
          border: OutlineInputBorder(),
        ),
        controller: apiKeyTextController,
        validator: (String? text) =>
            text!.isEmpty ? 'API key must not be empty' : null,
      ),
    ),
  );
}
