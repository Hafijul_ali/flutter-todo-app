# Open Todo

**A Free and Open Source, multi-platform Todo application that can save todos over cloud and sync them across devices.**

## About
Open Todo app is a free and opensource alternative to cloud based task managing applications. Todos are saved over user specified cloud services like Google Drive, Microsoft One Drive, Dropbox, etc. Users need to authenticate their cloud service providers with the app, and the app periodically syncs the todo with cloud storage. This approach enables to have a unified cross platform experience, essentially enabling users to keep track of their todo items from any of their devices, tailored for an individual.

## Features
+ ### Cross Platform
&emsp; &emsp; &emsp; Open Todo has multiple clients covering all major platforms like Android, iOS, Windows, etc.

+ ### Cloud Based
&emsp; &emsp; &emsp; Open Todo saves todo locally as well as on cloud, so todos are transported to the cloud whenever device connects to internet.

+ ### Material UI
&emsp; &emsp; &emsp; Open Todo is built on top of Google's Flutter UI Toolkit that has inbuilt support for Maetrial UI, which feels modern.

+ ### Local Database
&emsp; &emsp; &emsp; Open Todo saves todo locally with the help of Hive, which is a high performance, easy to use NoSQL database.

+ ### Notifications and Alerts
&emsp; &emsp; &emsp; Open Todo not only saves the todo, but also notifies users prior to the todo deadline.

## Getting Started

The application can be downloaded and tested for various platforms using the links given below

### Windows
Download the Windows application from this link

### Android
Download the Android application from this link

### iOS
Download the iOS application from this link

### MacOS
Download the MacOS application from this link

### Linux
Download the Linux application from this link

### Web App
Web App of OpenTodo is accessible at
<br/> &emsp; - https://hafijulali.gitlab.io/open-todo/#/ [latest version built directly from master brach]
<br/> &emsp; - https://apps.hafijul.dev/open-todo
<br/> &emsp; - https://app.opentodo.repl.co
<br/> &emsp; - https://www.opentodo.repl.co
<br/> &emsp; - https://opentodo.glitch.me

## Building from source
### Android
  - Download and install flutter and related build tools like gradle and jdk.
  - Go to the project root by cd [project].
  - `flutter build apk --release`  for building apk.
### iOS
  - Download and install flutter and related build tools for iOS.
  - Go to the project root by cd [project].
  - `flutter build ios --release`  for building ios app.
### Linux
  - Download and install flutter and related build tools like cmake and ninja.
  - Go to the project root by cd [project].
  - `flutter build linux --release`  for building linux executable bundle.
### MacOS
  - Download and install flutter and related build tools for MacOS.
  - Go to the project root by cd [project].
  - `flutter build macos --release`  for building macos executable bundle.
### Windows
  - Download and install flutter and related build tools like cmake and windows-sdk.
  - Go to the project root by cd [project].
  - `flutter build windows --release`  for building windows executable bundle.
### Web
  - Download and install flutter and related build tools for Web.
  - Go to the project root by cd [project].
  - `flutter build web --release`  for building javascript app

## For developers





## License
This project is licensed under [Apache License2.0](https://apache.org/licenses/LICENSE-2.0).


## Contributing
Project contribution can be made  by forking the repository and making a pull request. The merge code will be thoroughly tested and then merged to respective branch(except master). 

## Project Status
The project is being actively developed by the author(s) depending upon their availability. We as community can make opensource software accessible and reliable for all.
